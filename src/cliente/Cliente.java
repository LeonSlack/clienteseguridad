/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nupy
 */
public class Cliente {

    private static Socket sock;
    private static BufferedReader stdin;
    private static PrintStream os;
    private static String ipServidorCentral="192.168.0.103";//192.168.0.100
    private static KeyGen key= new KeyGen();
    private static String KS;
    private static PublicKey PublicaServidor;
    private static boolean tengopub=false;

    public static void main(String[] args) throws IOException {
        key.generateKeys();       
        try {
            
            sock = new Socket(ipServidorCentral, 4444);
            stdin = new BufferedReader(new InputStreamReader(System.in));
            System.out.println ("Se a conectado al servidor con exito!");
        } catch (Exception e) {
            System.err.println("Cannot connect to the server, try again later.");
            System.exit(1);
        }

        os = new PrintStream(sock.getOutputStream());
        DataInputStream is = new DataInputStream(sock.getInputStream());
        os.println("no");
        //empiezo intercambio de publicas
        try {        
            System.out.println("entre en el trai");
            //Recibo la clave publica del servidor
            byte[] lenb = new byte[4];
            is.read(lenb,0,4);
            ByteBuffer bb = ByteBuffer.wrap(lenb);
            int len = bb.getInt();
            byte[] servPubKeyBytes = new byte[len];
            is.read(servPubKeyBytes);
            X509EncodedKeySpec k = new X509EncodedKeySpec(servPubKeyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicaServidor = kf.generatePublic(k);
            //System.out.println(PublicaServidor);
            //envio mi publica
            bb = ByteBuffer.allocate(4);
            bb.putInt(key.getPublicKey().getEncoded().length);
            os.write(bb.array());
            os.write(key.getPublicKey().getEncoded());
            os.flush();
            //System.out.println(key.getPublicKey());
            KS=key.desencriptarConPublicaExterna(is.readUTF(), PublicaServidor);
            System.out.println(KS);
           
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        //
        String inicio=opcion_inicial();
        boolean log =false; //este te dira si entras o no
        while (inicio.equals("SALIR")==false)

        {    
            if (inicio.equals("INSCRIPCION")){
                os = new PrintStream(sock.getOutputStream());
                os.println("INSCRIPCION");// envio opcion 
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Introduzca nombre de usuario: ");
                String nombre = br.readLine();
                BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Introduzca contraseña: ");
                String clave = br2.readLine();
                os.println(key.EncriptarKS(nombre,KS));// envio cliente nombre
                os.println(key.EncriptarKS(clave,KS));// envio clave nombre
                InputStream in = sock.getInputStream();
                DataInputStream clientData = new DataInputStream(in);
                int reg = Integer.parseInt(clientData.readUTF());// respuesta acceso servidor
                    if(reg==1){
                    System.out.println("Usuario agregado con exito");
                    inicio=opcion_inicial();
                    }
                    else{
                    System.err.println("El usuario ya se encuentra reguistrado");

                    inicio=opcion_inicial();
                    }

            }
            else if (inicio.equals("ENTRAR")){
                os = new PrintStream(sock.getOutputStream());
                os.println("ENTRAR");// envio opcion a servidor
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Introduzca nombre de usuario: ");
                String nombre = br.readLine();
                BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Introduzca contraseña: ");
                String clave = br2.readLine();
                os.println(key.EncriptarKS(nombre,KS));// envio cliente nombre
                os.println(key.EncriptarKS(clave,KS));// envio clave nombre
                InputStream in = sock.getInputStream();
                DataInputStream clientData = new DataInputStream(in);
                int acceso = Integer.parseInt(clientData.readUTF());// respuesta acceso servidor

                if(acceso==1){
                log=true;    
                }

                if (log == true){
                    try {

                        Integer i=-1;
                        
                        while (i!=0){
                            //DataOutputStream ps = new DataOutputStream(sock.getOutputStream());
                            os = new PrintStream(sock.getOutputStream());
                            os.println("si"); //le indico al servidor que ya tengo publica
                            os.println("entro");
                            DataInputStream IS = new DataInputStream(sock.getInputStream());
                            String pelicula = IS.readUTF();
                            Integer numeroPelicula =0;
                            //aqui para leer lo que me envia el SC
                            while (pelicula.matches("fin")==false){
                                if (pelicula!="fin"){
                                    numeroPelicula++;
                                    System.out.println(numeroPelicula+". "+pelicula);

                                }
                                pelicula=IS.readUTF();
                            
                            }
                            //
                            System.out.println("Seleccione un video escribiendo el numero del video");
                            System.out.println("0 Para salir.");
                            i= Integer.parseInt(stdin.readLine());
                            os.write(i);
                            if (i>0)
                            {
                                
                                String nombreVideo = IS.readUTF();
                                String respuesta=IS.readUTF();
                                numeroPelicula=0;
                                System.out.println(nombreVideo+" se encuentra en el/los servidor/es");
                                List ip = new ArrayList();
                                List port= new ArrayList();
                                while (respuesta.matches("fin")==false)
                                {
                                    if (respuesta!="fin")
                                    {
                                        numeroPelicula++;
                                        String p = IS.readUTF();
                                        System.out.println(numeroPelicula+". "+respuesta+" puerto: "+p);
                                        ip.add(respuesta);
                                        port.add(p);
                                    }
                                    respuesta=IS.readUTF();
                                }
                                System.out.println("Seleccione un servidor escribiendo el numero del video");
                                System.out.println("0 Para salir.");
                                i= Integer.parseInt(stdin.readLine());
                                if (i>0)
                                {
                                    //os.write(i);
                                    String ipS = ip.get(i-1).toString();
                                    int portS = Integer.parseInt(port.get(i-1).toString());
                                    System.out.println(ipS+" "+portS);    
                                    receiveFile(ipS,portS,nombreVideo);
                                    i=-1;
                                    sock = new Socket(ipServidorCentral, 4444);
                                }
                            }
                        }    
                    } catch (Exception e) {
                        System.err.println("Error: "+e);
                    }
                    inicio=opcion_inicial();
                }
                else 
                {
                    System.err.println("Contraseña incorrecta");
                    inicio=opcion_inicial();
                }
            }
            else
               inicio=opcion_inicial(); 
        } 
       
       os.println("SALIR");
       sock.close();

    }
    
    public static String opcion_inicial() throws IOException 
    {
        System.out.println("1. INSCRIPCION");
        System.out.println("2. ENTRAR");
        System.out.println("0. SALIR.");
        System.out.print("\nIngrese un comando: ");

        return stdin.readLine();
    }
    
    public static void receiveFile(String ip,int p,String fileName) {
        
        try {
            sock.close();
            sock = new Socket(ip,p);
            //Socket servidor = new Socket(ip,p);
            int bytesRead;
            InputStream in = sock.getInputStream();
            DataInputStream clientData = new DataInputStream(in);
            ////apatado1
            fileName = "C:\\server\\"+fileName;
            PrintStream os= new PrintStream(sock.getOutputStream());
            os.println(fileName);
            fileName = clientData.readUTF();//desencripto con la publica de el                  
            OutputStream output = new FileOutputStream(fileName);
            double size = clientData.readLong();
            double i = size;
            String restante_prev="";
            String restante="";
            byte[] buffer = new byte[1024];      
            while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                output.write(buffer, 0, bytesRead);
                size -= bytesRead;
                DecimalFormat decimales = new DecimalFormat ("0.00");
                restante = decimales.format((((size*100)/i) -100)* -1);
                if (restante.equals(restante_prev)==false)
                {
                    System.out.println("Descargando: "+fileName+", Proceso: "+restante+"%");
                    restante_prev=restante;
                }
            }
            output.close();
            in.close();
            sock.close();
            System.out.println(fileName+" Recibido del servidor: "+ip);
            
        } catch (IOException ex) {
            System.out.println(ex);
        } 
    }

    
}
